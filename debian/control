Source: wxpython4.0
Section: python
Priority: optional
Maintainer: wxWidgets Maintainers <team+wx@tracker.debian.org>
Uploaders: Scott Talbert <swt@techie.net>
Build-Depends:
    debhelper-compat (= 13),
    dh-python,
    doxygen,
    libgtk-3-dev,
    libwxgtk3.0-gtk3-dev,
    libwxgtk-media3.0-gtk3-dev,
    libwxgtk-webview3.0-gtk3-dev,
    python3-all,
    python3-all-dev,
    python3-numpy,
    python3-pil,
    python3-pypdf2,
    python3-pytest,
    python3-pytest-timeout,
    python3-pytest-xdist,
    python3-setuptools,
    python3-sipbuild,
    python3-six,
    sip-tools,
    xauth,
    xvfb,
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/freewx-team/wxpython4.0.git
Vcs-Browser: https://salsa.debian.org/freewx-team/wxpython4.0
Homepage: https://www.wxpython.org/

Package: python3-wxgtk4.0
Architecture: any
Depends: python3-pil, python3-six, ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends}
Suggests: wx3.0-doc
Provides: ${python3:Provides}
Description: Python 3 interface to the wxWidgets Cross-platform C++ GUI toolkit
 wxWidgets (formerly known as wxWindows) is a class library for C++ providing
 GUI components and other facilities on several popular platforms (and some
 unpopular ones as well).
 .
 This package provides a Python 3 interface to the wxGTK library and the
 wxPython runtime support libraries.  This is the Phoenix reimplementation of
 wxPython.

Package: python3-wxgtk-media4.0
Architecture: any
Depends: python3-wxgtk4.0 (= ${binary:Version}), ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends}
Provides: ${python3:Provides}
Description: Python 3 interface to the wxWidgets Cross-platform C++ GUI toolkit (wx.media)
 wxWidgets (formerly known as wxWindows) is a class library for C++ providing
 GUI components and other facilities on several popular platforms (and some
 unpopular ones as well).
 .
 This package provides a Python 3 interface to wxMediaCtrl.  This is the Phoenix
 reimplementation of wxPython.

Package: python3-wxgtk-webview4.0
Architecture: any
Depends: python3-wxgtk4.0 (= ${binary:Version}), ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends}
Provides: ${python3:Provides}
Description: Python 3 interface to the wxWidgets Cross-platform C++ GUI toolkit (wx.html2)
 wxWidgets (formerly known as wxWindows) is a class library for C++ providing
 GUI components and other facilities on several popular platforms (and some
 unpopular ones as well).
 .
 This package provides a Python 3 interface to wxWebView.  This is the Phoenix
 reimplementation of wxPython.

Package: wxpython-tools
Architecture: all
Depends: python3-wxgtk4.0, ${python3:Depends}, ${misc:Depends}
Provides: python-wxtools
Replaces: python-wxtools
Conflicts: python-wxtools
Description: Tools from the wxPython distribution
 wxWidgets (formerly known as wxWindows) is a class library for C++ providing
 GUI components and other facilities on several popular platforms (and some
 unpopular ones as well).
 .
 This package provides support utilities and common files for wxPython.
